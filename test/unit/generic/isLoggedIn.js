var expect = require('chai').expect
var isLoggedInMW = require('../../../src/middleware/auth/isLoggedIn')

describe('auth MW', function() {
    it('should call next if the userid in session exists', function(done) {

        var reqMock = {
            session: {
                userid: 'joe'
            }
        }
        isLoggedInMW({})(reqMock, {}, function() {
            done()
        })
    })

    it('should call res.redirect', function(done) {

        var reqMock = {
            session: {}
        }
        var resMock = {
            redirect: function (newUrl) {
                expect(newUrl).be.eql('/')
                done()
            }
        }

        isLoggedInMW({})(reqMock, resMock, function() {
            expect('next should not be called').be.eql(false)
        })
    })
    
})