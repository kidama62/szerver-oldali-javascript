var expect = require('chai').expect
var createUserMW = require('../../../src/middleware/user/createUser')

describe('createUser MW', function() {
    it('should call next if req.body.firstName doesnt exist', function(done) {
        var reqMock = {
            body: {
                lastName: 'Kiss',
                birthdate: new Date('December 17, 1995 03:24:00'),
                sex: 'male',
                address: 'Budapest, Tűzoltó u. 66, 1094'
            }
        }
        var resMock = {
            tpl: {

            }
        }
        createUserMW({userModel: true})(reqMock, resMock, function() {
            done()
        })
    })
    it('should call next if req.body.lastName doesnt exist', function(done) {
        var reqMock = {
            body: {
                firstName: 'Andras',
                birthdate: new Date('December 17, 1995 03:24:00'),
                sex: 'male',
                address: 'Budapest, Tűzoltó u. 66, 1094'
            }
        }
        var resMock = {
            tpl: {

            }
        }
        createUserMW({userModel: true})(reqMock, resMock, function() {
            done()
        })
    })
    it('should call next if req.body.birthdate doesnt exist', function(done) {
        var reqMock = {
            body: {
                firstName: 'Andras',
                lastName: 'Kiss',
                sex: 'male',
                address: 'Budapest, Tűzoltó u. 66, 1094'
            }
        }
        var resMock = {
            tpl: {

            }
        }
        createUserMW({userModel: true})(reqMock, resMock, function() {
            done()
        })
    })
    it('should call next if req.body.sex doesnt exist', function(done) {
        var reqMock = {
            body: {
                firstName: 'Andras',
                lastName: 'Kiss',
                birthdate: new Date('December 17, 1995 03:24:00'),
                address: 'Budapest, Tűzoltó u. 66, 1094'
            }
        }
        var resMock = {
            tpl: {

            }
        }
        createUserMW({userModel: true})(reqMock, resMock, function() {
            done()
        })
    })
    it('should call next if req.body.address doesnt exist', function(done) {
        var reqMock = {
            body: {
                firstName: 'Andras',
                lastName: 'Kiss',
                birthdate: new Date('December 17, 1995 03:24:00'),
                sex: 'male'
            }
        }
        var resMock = {
            tpl: {

            }
        }
        createUserMW({userModel: true})(reqMock, resMock, function() {
            done()
        })
    })
    it('should make a user and redirect to /list-user', function(done) {
        var reqMock = {
            body: {
                firstName: 'Andras',
                lastName: 'Kiss',
                birthdate: new Date('December 17, 1995 03:24:00'),
                sex: 'male',
                address: 'Budapest, Tűzoltó u. 66, 1094'
            }
        }
        
        var resMock = {
            tpl: {},
            redirect: function(path) {
                expect(path).be.eql('/list-user')
                done()
            }
        }

        var userMock = function() {

        }

        userMock.prototype.save = function(cb) {
            cb(undefined)
        }

        createUserMW({userModel: userMock})(reqMock, resMock, function() {
            done()
        })
    })
    it('should call next if user.save returns err', function(done) {
        var reqMock = {
            body: {
                firstName: 'Andras',
                lastName: 'Kiss',
                birthdate: new Date('December 17, 1995 03:24:00'),
                sex: 'male',
                address: 'Budapest, Tűzoltó u. 66, 1094'
            }
        }
        
        var resMock = {
            tpl: {},
            redirect: function(path) {
                expect(path).be.eql('/list-user')
                done()
            }
        }

        var userMock = function() {

        }

        userMock.prototype.save = function(cb) {
            cb('randomstring')
        }

        var nextMock = function(nextValue) {
            expect(nextValue).be.eql('randomstring')
            done()
        }

        createUserMW({userModel: userMock})(reqMock, resMock, nextMock)
    })
})