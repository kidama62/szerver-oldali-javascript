var db = require('../config/db');

var Crypto = db.model('Crypto', {
  name: String,
  ticker: String,
  value: Number,
});

module.exports = Crypto;