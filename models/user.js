var db = require('../config/db');

var User = db.model('User', {
  firstName: String,
  lastName: String,
  birthDate: Date,
  sex: String,
  address: String,
  registrationDate: Date,
  cryptos: Array
});

module.exports = User;