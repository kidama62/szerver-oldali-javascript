module.exports = {
  firstName: 'John',
  lastName: 'Doe',
  birthDate: Date('1992-11-06'),
  sex: 'male',
  address: 'Biatorbagy Majus 1. str. 31.',
  registrationDate: Date('2018-12-03'),
  cryptos: []
}
