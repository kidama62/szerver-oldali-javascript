const { Seeder } = require('mongo-seeding');
const path = require('path');

const config = {
    database: 'mongodb://localhost/crypto',
    dropDatabase: true,
};

const seeder = new Seeder(config);

const collections = seeder.readCollectionsFromPath(path.resolve("./seed/collection"));

async function seed() {
    try {
        await seeder.import(collections);

    } catch (err) {
        // Handle errors
    }
}

seed();
