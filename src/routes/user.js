var renderMW = require('../middleware/generic/render');
var createUserMW = require('../middleware/user/createUser');
var isLoggedInMW = require('../middleware/auth/isLoggedIn');
var listUsersMW = require('../middleware/user/listUsers');
var deleteUserMW = require('../middleware/user/deleteUser');
var findUserMW = require('../middleware/user/findUser');
var addCryptoToUserMW = require('../middleware/user/addCryptoToUser');
var listCryptosMW = require('../middleware/crypto/listCryptos');
var calcCryptoSumMW = require('../middleware/crypto/calcCryptoSum');

var userModel = require('../../models/user');
var cryptoModel = require('../../models/crypto');

module.exports = function (app) {

    var objectRepository = {
        userModel: userModel,
        cryptoModel: cryptoModel
    };

    /**
     * List users
     */
    app.get('/list-user',
        isLoggedInMW(objectRepository),
        listUsersMW(objectRepository),
        renderMW(objectRepository, 'list-user')
    );

    /**
     * Create users
     */
    app.post('/create-user',
        isLoggedInMW(objectRepository),
        createUserMW(objectRepository),
        function(req, res, next) {
            return res.redirect('/list-user')
        }
    );

    /**
     * Show user
     */
    app.get('/user/:userid',
        isLoggedInMW(objectRepository),
        listCryptosMW(objectRepository),
        findUserMW(objectRepository),
        calcCryptoSumMW(objectRepository),
        renderMW(objectRepository, 'single-user')
    );

    /**
     * Delete user
     */
    app.get('/user/delete/:userid',
        isLoggedInMW(objectRepository),
        deleteUserMW(objectRepository),
        function(req, res, next) {
            return res.redirect('/list-user')
        }
    );

    /**
     * Add crypto to user
     */
    app.post('/user/add-crypto/:userid',
        isLoggedInMW(objectRepository),
        findUserMW(objectRepository),
        addCryptoToUserMW(objectRepository),
        function(req, res, next) {
            return res.redirect('/user/' + res.tpl.user._id)
        }
    );
};
