var renderMW = require('../middleware/generic/render');
var inverseIsLoggedInMW = require('../middleware/auth/inverseIsLoggedIn');
var postUserLoginMW = require('../middleware/auth/postUserLogin');
var mainRedirectMW = require('../middleware/auth/mainRedirect');
var logUserOutMW = require('../middleware/auth/logUserOut');

module.exports = function (app) {

    var objectRepository = { };

    /**
     * Main page
     */
    app.get('/',
        mainRedirectMW(objectRepository)
    );

    /**
     * Render login
     */
    app.get('/login',
        inverseIsLoggedInMW(objectRepository),
        renderMW(objectRepository, 'login')
    );

    /**
     * Login screen post route
     */
    app.post('/login',
        postUserLoginMW(objectRepository),
        renderMW(objectRepository, 'login')
    );

    /**
     * Logout post route
     */
    app.get('/logout',
        logUserOutMW(objectRepository)
    );

};
