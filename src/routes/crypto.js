var renderMW = require('../middleware/generic/render');
var isLoggedInMW = require('../middleware/auth/isLoggedIn');
var listCryptosMW = require('../middleware/crypto/listCryptos');
var editCryptoMW = require('../middleware/crypto/editCrypto');

var cryptoModel = require('../../models/crypto');

module.exports = function (app) {

    var objectRepository = {
        cryptoModel: cryptoModel
    };

    /**
     * Crypto list 
     */
    app.get('/list-crypto',
        isLoggedInMW(objectRepository),
        listCryptosMW(objectRepository),
        renderMW(objectRepository, 'list-crypto')
    );

    /**
     * Crypto edit
     */
    app.get('/edit-crypto',
        isLoggedInMW(objectRepository),
        editCryptoMW(objectRepository),
        function(req, res, next) {
            return res.json(res.locals.crypto)
        }
    );

};
