var loadDependency = require('../generic/loadDependency').loadDependency;

/**
 * Lists the cryptocurrencies
 */
module.exports = function (objectrepository, viewName) {

    var cryptoModel = loadDependency(objectrepository, 'cryptoModel');

    return function (req, res, next) {
        cryptoModel.find({ }).exec(function (err, results) {
            if (err) {
              return next(err);
            }
      
            res.tpl.cryptoArray = results;
            return next();
          });
    };

};