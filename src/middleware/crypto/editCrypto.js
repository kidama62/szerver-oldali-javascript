var loadDependency = require('../generic/loadDependency').loadDependency;

/**
 * Edit cryptocurrency
 */
module.exports = function (objectrepository, viewName) {

    var CryptoModel = loadDependency(objectrepository, 'cryptoModel');

    return function (req, res, next) {
        if(typeof req.param('_id') === 'undefined' || typeof req.param('name') === 'undefined' ||
        typeof req.param('ticker') === 'undefined' || typeof req.param('value') === 'undefined') {
            return next();
        }

        CryptoModel.findById(req.param('_id'), function(err, crypto) {
            crypto.name = req.param('name')
            crypto.ticker = req.param('ticker')
            crypto.value = req.param('value')

            crypto.save(function(err, result) {
                if (err) {
                    return next(err);
                }

                res.locals.crypto = result

                return next();
            })
        })
    };

};