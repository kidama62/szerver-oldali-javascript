var loadDependency = require('../generic/loadDependency').loadDependency;

/**
 * Add crypto to user
 */
module.exports = function (objectrepository, viewName) {

    return function (req, res, next) {

        if (typeof res.tpl.user === 'undefined') {
            return next();
        }

        user = res.tpl.user;
        var found = undefined

        user.cryptos.forEach(function(element, index, cryptoArray) {
            if(element.cryptoTypeId === req.body.cryptoTypeId) {
                found = true
                cryptoArray.set(index, {
                    cryptoTypeId: element.cryptoTypeId,
                    cryptoAmount: req.body.cryptoAmount
                })
            }
        });

        if (!found) {
            user.cryptos.push({
                cryptoTypeId: req.body.cryptoTypeId,
                cryptoAmount: req.body.cryptoAmount
            });
        }

        user.save(function (err, result) {
            if (err) {
              return next(err);
            }

            return next();
        });

    };

};