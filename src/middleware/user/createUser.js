var loadDependency = require('../generic/loadDependency').loadDependency;

/**
 * Lists the cryptocurrencies
 */
module.exports = function (objectrepository, viewName) {

    var userModel = loadDependency(objectrepository, 'userModel');

    return function (req, res, next) {

        if( typeof req.body.firstName === 'undefined' || typeof req.body.lastName === 'undefined' ||
            typeof req.body.birthdate === 'undefined' || typeof req.body.sex === 'undefined' ||
            typeof req.body.address === 'undefined' ) {
            res.tpl.error = 'You must fill every field.'
            return next();
        }

        var newUser = new userModel()
        newUser.firstName = req.body.firstName
        newUser.lastName = req.body.lastName
        newUser.birthDate = req.body.birthdate
        newUser.sex = req.body.sex
        newUser.address = req.body.address
        newUser.registrationDate = Date.now()
        newUser.cryptos = []

        newUser.save(function (err) {
            if(err) {
                return next(err);
            }
            return res.redirect('/list-user');
        });

    };

};