var loadDependency = require('../generic/loadDependency').loadDependency;

/**
 * Lists the cryptocurrencies
 */
module.exports = function (objectrepository, viewName) {

    var userModel = loadDependency(objectrepository, 'userModel');

    return function (req, res, next) {

        userModel.find({ }).exec(function (err, results) {
            if (err) {
              return next(err);
            }
      
            res.tpl.users = results;
            return next();
        });

    };

};