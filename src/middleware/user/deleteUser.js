var loadDependency = require('../generic/loadDependency').loadDependency;

/**
 * Lists the cryptocurrencies
 */
module.exports = function (objectrepository, viewName) {

    var userModel = loadDependency(objectrepository, 'userModel');

    return function (req, res, next) {

        userModel.findOne({
            _id: req.param('userid')
        }).exec(function (err, result) {
            if ((err) || (!result)) {
              return next();
            }

            result.remove(function (err) {
                if (err) {
                  return next(err);
                }
                
            });
        });

        return next();

    };

};