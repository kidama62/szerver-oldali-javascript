var loadDependency = require('../generic/loadDependency').loadDependency;

function calculateAge(birthday) { // birthday is a date
    var ageDifMs = Date.now() - birthday.getTime();
    var ageDate = new Date(ageDifMs); // miliseconds from epoch
    return Math.abs(ageDate.getUTCFullYear() - 1970);
}

/**
 * Lists the cryptocurrencies
 */
module.exports = function (objectrepository, viewName) {

    var userModel = loadDependency(objectrepository, 'userModel');

    return function (req, res, next) {

        userModel.findOne({
            _id: req.param('userid')
        }).exec(function (err, result) {
            if ((err) || (!result)) {
              return next();
            }

            res.tpl.user = result;
            res.tpl.age = calculateAge(result.birthDate)
            return next();
        });

    };

};