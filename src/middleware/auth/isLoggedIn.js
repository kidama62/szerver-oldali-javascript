/**
 * If user is logged in the redirects to crypto list page otherwise next
 */

module.exports = function (objectrepository, viewName) {

    return function (req, res, next) {
        if (typeof req.session.userid === 'undefined') {
            return res.redirect('/');
        }
        return next();
    };

};