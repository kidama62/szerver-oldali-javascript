/**
 * Inverse isLoggedIn
 */

module.exports = function (objectrepository, viewName) {

    return function (req, res, next) {
        if (typeof req.session.userid !== 'undefined') {
            return res.redirect('/list-crypto');
        }
        return next();
    };

};