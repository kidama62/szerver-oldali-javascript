/**
 * If the user knows the right credentials then logs we
 * log it in to the system
 */
module.exports = function (objectrepository) {

  return function (req, res, next) {

      //not enough parameter
      if ((typeof req.body === 'undefined') || (typeof req.body.email === 'undefined') ||
        (typeof req.body.password === 'undefined')) {
        return next();
      }

      //check credentials
      if (req.body.email !== 'admin@gmail.com' || req.body.password !== 'sajt') {
        res.tpl.error.push('Wrong password!');
        return next();
      }

      //login is ok, save id to session
      req.session.userid = 'SuperLongHash';
      
      //redirect to / so the app can decide where to go next
      return res.redirect('/');
    };

};
