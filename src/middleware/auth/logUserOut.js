/**
 * Logs the user out
 */

module.exports = function (objectrepository, viewName) {

    return function (req, res, next) {
        req.session.destroy(function (err) {
            return res.redirect('/');
        });
    };

};