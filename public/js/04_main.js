"use strict";

(function($){

    $(document).ready(function() {

        ///////////////////////////////
        // list-crypto
        ///////////////////////////////
        $('.crypto-row__edit-btn').click(function() {
            var $cryptoRow = $(this).closest('.crypto-row')
            var $cancelBtn = $cryptoRow.find('.crypto-row__cancel-btn')
            var $saveBtn = $cryptoRow.find('.crypto-row__save-btn')

            $cryptoRow.find('.crypto-row__input').addClass('crypto-row__input--shown')
            $cryptoRow.find('.crypto-row__p').addClass('crypto-row__p--hidden')

            $cancelBtn.removeClass('btn--hidden')
            $saveBtn.removeClass('btn--hidden')
            $(this).addClass('btn--hidden')
        })

        $('.crypto-row__cancel-btn').click(cancelFn)
        
        function cancelFn() {
            var $cryptoRow = $(this).closest('.crypto-row')
            var $editBtn = $cryptoRow.find('.crypto-row__edit-btn')
            var $saveBtn = $cryptoRow.find('.crypto-row__save-btn')

            $cryptoRow.find('.crypto-row__input').removeClass('crypto-row__input--shown')
            $cryptoRow.find('.crypto-row__p').removeClass('crypto-row__p--hidden')

            $editBtn.removeClass('btn--hidden')
            $saveBtn.addClass('btn--hidden')
            $(this).addClass('btn--hidden')
        }

        $('.crypto-row__save-btn').click(function() {
            var $cryptoRow = $(this).closest('.crypto-row')
            var $cancelBtn = $cryptoRow.find('.crypto-row__cancel-btn')
            var $inputFields = $cryptoRow.find('.crypto-row__input')
            var dataToSend = {
                _id: $cryptoRow.data('id')
            }

            $.each($inputFields, function(index, value) {
                var $input = $(value)
                dataToSend[$input.data('property')] = $input.val()
            })
            

            $.ajax({
                url: '/edit-crypto',
                data: dataToSend
            })
            .done(function(data) {
                $cryptoRow.find('[data-property="name"]').val(data.name).text(data.name)
                $cryptoRow.find('[data-property="ticker"]').val(data.ticker).text(data.ticker)
                $cryptoRow.find('[data-property="value"]').val(data.value).text(data.value)

                cancelFn.call($cancelBtn)
            })
        })

        ///////////////////////////////
        // single-user
        ///////////////////////////////
        $('.add-crypto-form__show-btn').click(function() {
            $('.add-crypto-form__input-wrapper').removeClass('add-crypto-form__input-wrapper--hidden')
            $(this).addClass('btn--hidden')
            $('.add-crypto-form__submit-btn').removeClass('btn--hidden')
        })

    })

})(jQuery)